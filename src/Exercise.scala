object Exercise extends App {

  def neg(predicate: String => Boolean): String => Boolean =
    !predicate(_)

  val neg2: (String => Boolean) => String => Boolean = predicate =>
    !predicate(_)

  val notEmpty = neg2(_ == "")
  println(notEmpty("Asd"))


  def parity(x: Int): String = x match {
    case n if n % 2 == 0 => "Even"
    case _ => "Odd"
  }

  val parity2: Int => String = /*x => x match... or _ match*/ {
    case n if n % 2 == 0 => "Even"
    case _ => "Odd"
  }

  val parity3: Int => String = x => if (x % 2 == 0) "Even" else "Odd"

  println(parity3(2))


  def p1(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  def p2(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z

  val p3: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  val p4: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z


  println(p1(2, 3, 4))
  println(p2(2)(3)(4))
  println(p3(2, 3, 4))
  println(p4(2)(3)(4))
  println(p1(4, 3, 4))
  println(p2(4)(3)(4))
  println(p3(4, 3, 4))

  val a = p4(2)
  val b = a(3)
  val c = b(4)
  println(c)

  /* 4 */
  def compose(f: Int => Int, g: Int => Int): Int => Int =
    x => f(g(x)) //Perchè non va con f(g(_))?

  println(compose(_ - 1, _ * 2)(5))


  /* 5 */
  def fib(n: Int): Int = n match {
    case n if n == 0 => 0
    case n if n == 1 => 1
    case n if n >= 2 => fib(n - 1) + fib(n - 2)
  }

  def fib2(n: Int): Int = {
    @annotation.tailrec
    def _fib(x: Int, l: Int, r: Int): Int = x match {
      case n if n == 0 => 0
      case n if n == 1 => l
      case n if n >= 2 => _fib(n - 1, l + r, l)
    }

    _fib(n, 1, 0)
  }

  println((fib(0), fib(1), fib(2), fib(3), fib(4), fib(5)))
  println((fib2(0), fib2(1), fib2(2), fib2(3), fib2(4), fib2(5)))

  /* 6 */
  import Person._

  println(personToString(Teacher("mirko", "PPS")))
  println(personToString(Student("mario", 2016)))


  /* 7 */
  import List._

  println(filter(Cons(10, Cons(20, Nil())))(_ > 15))
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))(_.length <= 2))

  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 2))
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))), 5))

  println(map(Cons(10, Cons(20, Nil())))(_ + 1))
  println(map(Cons(10, Cons(20, Nil())))(":" + _ + ":"))


  /* 8 */
  println(max(Cons(10, Cons(25, Cons(20, Nil())))))
  println(max(Nil()))


  /* 9 */
  def courses(l: List[Person]): List[String] =
    map(filter(l)(p => p match {
      case Teacher(_, _) => true
      case _ => false
    }))(t => t match {
      case Teacher(_, c) => c
    })


  /* 10 */
  def genOp(b: (Int, Int) => Int, i: Int): Int => Int =
    b(_, i)

  val minus3 = genOp(_ - _, 3)
  val div2 = genOp(_ / _, 2)
  println(compose(div2, minus3)(30))


  /* 11 */
  val incBy: Int => Int => Int = x => genOp(_ + _, x)
  val plus7 = incBy(7)
  println(plus7(8))


  /* 12 */
  def foldLeft[A](l: List[A])(default: A)(op: (A, A) => A): A = l match {
    case Nil() => default
    case Cons(h, t) => op(foldLeft(t)(default)(op), h)
  }

  def foldRight[A](l: List[A])(default: A)(op: (A, A) => A): A = l match {
    case Nil() => default
    case Cons(h, t) => op(h, foldRight(t)(default)(op))
  }

  val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  println(foldLeft(lst)(0)(_ - _))
  println(foldRight(lst)(0)(_ - _))
}
