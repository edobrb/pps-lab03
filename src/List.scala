// A generic linkedlist
sealed trait List[E]

// a companion object (i.e., module ) for List
object List {

  case class Cons[E](head: E, tail: List[E]) extends List[E]

  case class Nil[E]() extends List[E]

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
    case (Cons(h, t), `l2`) => Cons(h, append(t, l2))
    case (`l1`, Cons(h, t)) => Cons(h, append(l1, t))
    case _ => Nil()
  }

  def filter[A](l: List[A])(predicate: A => Boolean): List[A] = (l, predicate) match {
    case (Cons(h, t), p) => if (p(h)) Cons(h, filter(t)(p)) else filter(t)(p)
    case _ => Nil()
  }

  def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
    case (l, n) if n <= 0 => l
    case (Cons(_, t), n) if n > 0 => drop(t, n - 1)
    case (_, _) => Nil()
  }

  def map[A, B](l: List[A])(f: A => B): List[B] = (l, f) match {
    case (Cons(h, t), f) => Cons(f(h), map(t)(f))
    case (_, _) => Nil()
  }

  def max(l: List[Int]): Option[Int] = {

    def max(l: List[Int], m: Int): Int = {
      (l, m) match {
        case (Cons(h, t), m) if m >= h => max(t, m)
        case (Cons(h, t), m) if m < h => max(t, h)
        case _ => m
      }
    }

    l match {
      case Nil() => Option.empty
      case Cons(h, t) => Option.apply(max(t, h))
    }

  }

}