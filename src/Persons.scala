sealed trait Person // sealed : no other impl . except Student , Teacher

object Person {

  case class Student(name: String, year: Int) extends Person

  case class Teacher(name: String, course: String) extends Person

  def name(p: Person): String = p match {
    case Student(n, _) => n
    case Teacher(n, _) => n
  }

  def personToString(p: Person): String = p match {
    case Student(n, y) => n + ": " + y
    case Teacher(n, c) => n + ": [" + c + "]"
  }

}