object Optionals extends App {

  sealed trait Option[A]

  object Option { //Modulo compagno

    case class Some[A](a: A) extends Option[A]

    case class None[A]() extends Option[A]

    def pass[A](value: A)(predicate: A => Boolean): Option[A] = value match {
      case x if predicate(value) => Some(x)
      case _ => None()
    }

    def pass2[A](predicate: A => Boolean)(value: A): Option[A] = value match {
      case x if predicate(value) => Some(x)
      case _ => None()
    }

  }

  import Option._

  val asd = pass2[Int](_ >= 0)(_)
  println(asd(2))
  println(asd(-3))
}
